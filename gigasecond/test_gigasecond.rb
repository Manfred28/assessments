require 'test/unit'
require './gigasecond.rb'

class GigasecondTester < Test::Unit::TestCase
  test '3rd Feb 2001 in string format returns correct date' do
    assert_equal(age_after_gigasecond('3rd Feb 2001'), DateTime.parse('12th Oct 2032 03:46:40+02:00'))
  end

  test '3rd Feb 2001 in DateTime format returns correct date' do
    assert_equal(age_after_gigasecond(DateTime.new(2001,2,3)), DateTime.parse('12th Oct 2032 03:46:40+02:00'))
  end
end
