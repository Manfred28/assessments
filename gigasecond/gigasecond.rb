require 'date'

def age_after_gigasecond(date_of_birth)
  date_of_birth = DateTime.parse(date_of_birth) if date_of_birth.is_a? String
  Time.at(date_of_birth.to_time.to_i + 1_000_000_000).to_datetime
end
