require 'test/unit'
require './leap_year.rb'

class LeapYearTester < Test::Unit::TestCase
  test '2000 is a leap year' do
    assert(leap_year?(2000))
  end

  test '500 is not a leap year' do
    assert_false(leap_year?(500))
  end

  test '504 is a leap year' do
    assert(leap_year?(504))
  end
end
