def leap_year?(year)
  if (year % 400).zero?
    return true
  elsif (year % 100).zero?
    return false
  elsif (year % 4).zero?
    return true
  end
  false
end
