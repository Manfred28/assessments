require 'test/unit'
require './pascal_triangle.rb'

class PascalTriangleTester < Test::Unit::TestCase
  test 'calculates 1 row correctly' do
    assert_equal(pascal_triangle(1), [[1]])
  end

  test 'calculates 3 rows correctly' do
    assert_equal(pascal_triangle(3), [[1], [1, 1], [1, 2, 1]])
  end
end
