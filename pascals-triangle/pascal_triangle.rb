def pascal_triangle(row_count)
  rows = [[1]]
  return rows if row_count == 1
  (1..row_count - 1).each do
    rows.push([])
    (0..rows[-2].count).each_with_index do |_, i|
      left = i.zero? ? 0 : rows[-2][i - 1]
      right = rows[-2][i] || 0
      rows.last.push(left + right)
    end
  end
  rows
end

p pascal_triangle(5)
